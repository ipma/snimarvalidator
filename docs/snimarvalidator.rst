snimarvalidator package
=======================

.. automodule:: snimarvalidator
    :members:


modules
-------

.. toctree::


   snimarvalidator.validator
   snimarvalidator.command_line

