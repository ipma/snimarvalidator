"""
:mod:`parrot` -- Dead parrot access
===================================

.. module:: parrot
   :platform: Unix
   :synopsis: Validate Metadata Files against the SNIMar Profile

.. moduleauthor:: Eduardo Castanho  <eduardo.castanho@ipma.pt>

.. note::

   Content of note.
"""
__version__ = "0.0.2.alpha"
