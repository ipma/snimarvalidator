# coding=utf-8
"""
 Module validator
"""
import re
import os
import textwrap
from datetime import datetime
from datetime import timedelta
from lxml import etree
from lxml import isoschematron
from lxml.etree import DocumentInvalid
import snimarvalidator

FILE_PATH = os.path.dirname(snimarvalidator.__file__)
SNIMAR_XML_SCHEMA_ENTRY_POINT = os.path.join(FILE_PATH, 'SNIMAR_PROFILE_XSD/gmd/gmd.xsd')
SNIMAR_SCHEMATRON = os.path.join(FILE_PATH, 'SNIMAR_SCHEMATRON/snimarISORestriction.sch')
LINE_SIZE = 200


class Validator(object):
    """
        Class that
    """

    def __init__(self):

        with open(SNIMAR_XML_SCHEMA_ENTRY_POINT, "r") as schema_file:
            self.xml_schema_validator = etree.XMLSchema(etree.parse(schema_file))

        # creating a set of auxiliary function to use inside schematron
        ns_aux_schematron = etree.FunctionNamespace("http://snimar.pt/customFunctions")
        ns_aux_schematron.prefix = "snm"
        ns_aux_schematron.update({
            'date_in_future': date_in_future,
            'datetime_in_future': datetime_in_future,
            'check_time_extension': check_time_extension,
            'is_inspire': is_inspire,
            'is_snimar': is_snimar,
            'is_serviceclassification': is_serviceclassification,
            'is_in_inspirecodeList': is_in_inspirecodelist,
            'is_in_serviceclassificationcodelist': is_in_serviceclass_codelist,
            'where_are_codelist_file': where_are_codelist_file
        })

        with open(SNIMAR_SCHEMATRON, "r") as scht_file:
            self.isoschematron = isoschematron.Schematron(etree.parse(scht_file), store_report=True,
                                                          store_xslt=True)

        self._error_list = []

    # This function slows down program execution for some reason.
    def validate(self, doc_etree):
        """
        performs the validation

        :param doc_etree: etree
        :type doc_etree: `ElementTree`
        :return: True if it is valid False if not
        :rtype: bool
        """
        try:
            self.xml_schema_validator.assertValid(doc_etree)
        except DocumentInvalid:
            error = self.xml_schema_validator.error_log.last_error
            error_message = full_namespace_remover(error.message)
            val_error = ValidationError(error.type_name, error_message, error.line, None, None)
            self._error_list.append(val_error)
            return False
        if self.isoschematron.validate(doc_etree):
            return True
        else:
            # extract errors from report and storing then in Error Manager
            report = etree.fromstring(str(self.isoschematron.validation_report))
            for node in report.xpath('*[@flag="FAIL"]'):  # selects all nodes reporting error
                # extract error id
                error_id = node.get("id", "NOT_DEFINED")
                # extracting problematic node location
                prob_pat = r'/\*\[local-name\(\)=\'([^\']+)\' and namespace-uri\(\)=\'([^\']+)\'\]'
                replace_pattern = r'/\g<2>:\g<1>'
                path = re.sub(prob_pat, replace_pattern, node.get("location"))
                # reduce namespaces
                name_pattern = r'http://(([^/|:]*)/)+([^:]+):'
                path = re.sub(name_pattern, r'\g<3>:', path)
                # extracting line
                error_line = doc_etree.xpath(node.get("location"))[0].sourceline
                # extracting reason
                reason = None
                for subnode in node.getchildren():
                    if subnode.tag == "{http://purl.oclc.org/dsdl/svrl}text":
                        reason = subnode.text
                    break

                # obtaining code excerpt
                code_excerpt = prepare_code_excerpt(doc_etree, node.get("location"))
                self._error_list.append(
                    ValidationError(error_id, reason, error_line, path, code_excerpt))
                return False

    def get_error_codes_list(self):
        """

        :return: list of errors codes
        :rtype: `list[str]`
        """
        report = []
        for error in self._error_list:
            report.append([error.get('snv_id', 'Unknown'), error.get('line', '-1')])
        return report

    def get_error_report(self):
        """
        :return: detailed report of errors
        :rtype: str
        """

        report = u""
        for error in self._error_list:
            report += error.__unicode__()
        return report


def prepare_code_excerpt(etree_doc, path_to_node):
    """

    Extracts a code excerpt located  at path_to_node

    :param etree_doc: the metadata tree
    :type etree_doc:  `lxml.etree.ElementTree`
    :param path_to_node: xpath for node str
    :type path_to_node: str
    :return: code excerpt
    :rtype: str
    """
    result = etree.tostring(etree_doc.xpath(path_to_node)[0], encoding=unicode,
                            xml_declaration=False, method='html')
    # remove namespaces declarations
    result = re.sub(r'xmlns:[^ |>]* ?>', '>', result)
    result = re.sub(r'xmlns:[^ ]* ', '', result)
    result = re.sub(r' >', '>', result)
    result = re.sub(r'(  )+', '', result)
    result = re.sub(r'^ *$\n', '', result, flags=re.MULTILINE)
    result = result.strip()
    # shortening example
    if len(result.split('\n')) > 10:
        result = '\n'.join(result.split('\n')[:6]) + \
                 '\n  (...)[TOO LONG TO DISPLAY!](...)\n' + \
                 '\n'.join(result.split('\n')[-4:])
    return result


def full_namespace_remover(path):
    """
    :param path: path with full namespaces
    :type path: str
    :return: path without full namespaces
    :rtype: str
    """
    path = re.sub(r'\{[^\}]*\/([^\/|\}]+)\}', r'\g<1>:', path)
    return path


class ValidationError(dict):
    """
     class that represents a validation error
     Based on a dict for usability purposes and redefinition of method __unicode__ and __str___
    """

    def __init__(self, sn_id, reason, line, path, code, **kwargs):
        """
        :param sn_id: error id
        :type sn_id: str
        :param reason:  description of error
        :type reason: str
        :param line: line of error
        :type line: int
        :param path: xpath to the error
        :type path: str
        :param code: excerpt of error code
        :type code: str
        """
        super(ValidationError, self).__init__(**kwargs)
        self['snv_id'] = sn_id
        self['reason'] = reason
        self['line'] = line
        self['path'] = path
        self['code_example'] = code

    def __unicode__(self):

        ret = [
            u'-' * min(70, LINE_SIZE),
            u'|ERROR [' + self[u'snv_id'] + u'] @Line:' + unicode(self[u'line']),
            u'-' * min(70, LINE_SIZE),
            u'|Reason:',
            unicode(self[u'reason'])
        ]

        if self[u'path'] is not None:
            ret.append(u'|Problematic node xpath:')
            ret.append(unicode(self[u'path']))
        if self[u'code_example'] is not None:
            ret.append(u'|Code excerpt: ')
            result = self[u'code_example']
            if result is not None and len(result.split(u'\n')) > 10:
                ret.append(u'\n'.join(result.split(u'\n')[:6]))
                ret.append(u'(...)[TOO LONG TO DISPLAY!](...)')
                ret.append(u'\n'.join(result.split(u'\n')[-4:]))
            ret.append(result)
        ret.append(u'-' * min(70, LINE_SIZE))
        i = 0
        while True:
            if len(ret[i]) > LINE_SIZE:
                buf = ret.pop(i)
                # ret[i] = buf[0: LINE_SIZE]
                # ret.insert(i + 1, "->" + buf[LINE_SIZE:-1])
                ret[i:i] = textwrap.wrap(buf, LINE_SIZE, subsequent_indent="  ",
                                         fix_sentence_endings=True)
            i += 1
            if i >= len(ret):
                break

        return "\n".join(ret)

    def __str__(self):
        ret = self.__unicode__().encode('utf-8')
        return ret


# """
#
# SCHEMATRON AUXILIARY FUNCTIONS
#
# """

inspire = [u'Sistemas de referência',
           u'Sistemas de quadrículas geográficas',
           u'Toponímia',
           u'Unidades administrativas',
           u'Endereços',
           u'Parcelas cadastrais',
           u'Redes de transporte',
           u'Hidrografia',
           u'Sítios protegidos',
           u'Altitude',
           u'Ocupação do solo',
           u'Ortoimagens',
           u'Geologia',
           u'Unidades estatísticas',
           u'Edifícios',
           u'Solo',
           u'Uso do solo',
           u'Saúde humana e segurança',
           u'Serviços de utilidade pública e do Estado',
           u'Instalações de monitorização do ambiente',
           u'Instalações industriais e de produção',
           u'Instalações agrícolas e aquícolas',
           u'Distribuição da população-demografia',
           u'Zonas de gestão/restrição/regulamentação e unidades de referência',
           u'Zonas de risco natural',
           u'Condições atmosféricas',
           u'Características geometeorológicas',
           u'Características oceanográficas',
           u'Regiões marinhas',
           u'Regiões biogeográficas',
           u'Habitats e biótopos',
           u'Distribuição das espécies',
           u'Recursos energéticos',
           u'Recursos minerais']

serviceclassification = [u'Serviços geográficos com interacção humana',
                         u'Visualizador de catálogo',
                         u'Visualizador geográfico',
                         u'Visualização de folhas de cálculo geográficas',
                         u'Editor do serviço',
                         u'Editor da definição de cadeias',
                         u'Gestor do fluxo de trabalho',
                         u'Editor de elementos geográficos',
                         u'Editor de símbolos geográficos',
                         u'Editor de generalização de elementos geográficos',
                         u'Visualizador da estrutura dos dados geográficos',
                         u'Serviço de gestão de informação/modelos geográficos',
                         u'Serviço de acesso a elementos geográficos',
                         u'Serviço de acesso a mapas',
                         u'Serviço de acesso a coberturas',
                         u'Serviço de descrição de sensores',
                         u'Serviço de acesso a produtos',
                         u'Serviço de tipos de elementos geográficos',
                         u'Serviço de catálogo',
                         u'Serviço de registo',
                         u'Serviço de repertório',
                         u'Serviço de gestão de encomendas',
                         u'Serviço de encomendas pendentes',
                         u'Serviços de gestão do fluxo de trabalho/tarefas geográficas',
                         u'Serviço de definição de cadeia',
                         u'Serviço de fluxo de trabalho',
                         u'Serviço de assinatura',
                         u'Serviços de processamento geográfico — elementos espaciais',
                         u'Serviço de conversão de coordenadas',
                         u'Serviço de transformação de coordenadas',
                         u'Serviço de conversão cobertura/vector',
                         u'Serviço de conversão de coordenadas de imagens',
                         u'Serviço de rectificação',
                         u'Serviço de ortorrectificação',
                         u'Serviço de ajustamento do modelo geométrico dos sensores',
                         u'Serviço de conversão de modelos geométricos das imagens',
                         u'Serviço de definição de subconjuntos',
                         u'Serviço de amostragem',
                         u'Serviço de modificação do seccionamento',
                         u'Serviço de medição das dimensões',
                         u'Serviços de manipulação de elementos geográficos',
                         u'Serviço de correspondência de elementos geográficos',
                         u'Serviço de generalização de elementos geográficos',
                         u'Serviço de determinação do itinerário',
                         u'Serviço de localização',
                         u'Serviço de análise de proximidade',
                         u'Serviços de processamento geográfico — elementos temáticos',
                         u'Serviço de cálculo de geoparâmetros',
                         u'Serviço de classificação temática',
                         u'Serviço de generalização de elementos geográficos',
                         u'Serviço de definição de subconjuntos',
                         u'Serviço de contagem geográfica',
                         u'Serviço de detecção de alterações',
                         u'Serviços de extracção de informação geográfica',
                         u'Serviço de processamento de imagens',
                         u'Serviço de redução de resolução',
                         u'Serviços de manipulação de imagens',
                         u'Serviços de compreensão de imagens',
                         u'Serviços de síntese de imagens',
                         u'Serviços de manipulação de imagens multibandas',
                         u'Serviço de detecção de objectos',
                         u'Serviço de geoidentificação',
                         u'Serviço de geocodificação',
                         u'Serviços de processamento geográfico — elementos temporais',
                         u'Serviço de transformação do sistema de referência temporal',
                         u'Serviço de definição de subconjuntos',
                         u'Serviço de amostragem',
                         u'Serviço de análise de proximidade temporal',
                         u'Serviços de processamento geográfico – metadados',
                         u'Serviço de cálculo estatístico',
                         u'Serviços de anotação geográfica',
                         u'Serviços de comunicação geográfica',
                         u'Serviço de codificação',
                         u'Serviço de transferência',
                         u'Serviço de compressão geográfica',
                         u'Serviço de conversão de formato geográfico',
                         u'Serviço de transmissão de mensagens',
                         u'Gestão remota de ficheiros e de executáveis']


def date_in_future(context, date):
    """
    Auxiliary method to be used inside schematron

    :param date: date to test
    :type date: str
    :return: true if date is in future, False otherwise
    :rtype: bool
    """
    test_date = datetime.strptime(date[0], "%Y-%m-%d")
    if test_date.date() >= datetime.today().date() + timedelta(days=1):
        return False
    else:
        return True


def datetime_in_future(context, datetime_t):
    """
    Auxiliary method to be used inside schematron

    :param datetime_t: datetime to test
    :type datetime_t: str
    :return: true if datetime is in future, False otherwise
    :rtype: bool
    """
    test_datetime = datetime.strptime(datetime_t[0], "%Y-%m-%dT%H:%M:%S")
    if test_datetime >= datetime.today() + timedelta(days=1):
        return False
    else:
        return True


def check_time_extension(context, begin, end):
    """
    Auxiliary method to be used inside schematron

    :param begin: begin datetime
    :type begin: str
    :param end: end datetime
    :type end: str
    :return: True if its a valid time extension (begin < end ),False otherwise
    :rtype: bool
    """
    begin = datetime.strptime(begin[0], "%Y-%m-%dT%H:%M:%S")
    end = datetime.strptime(end[0], "%Y-%m-%dT%H:%M:%S")
    if begin > end:
        return False
    else:
        return True


def to_lower(context, string):
    """
    Auxiliary method to be used inside schematron

    :param string:
    :type string: `list[str]`
    :return:  a lower case str
    :rtype: str
    """
    if string is None:
        return ''
    return unicode(string[0]).lower()


def is_inspire(context, name):
    """
    Auxiliary method to be used inside schematron

    :param name: keyword to test
    :type name: `list[str]`
    :return: True if name matches to INSPIRE/GEMET name, False otherwise
    :rtype: bool
    """
    if name is None:
        return False
    # print string, "is inspire?", 'gemet' in string[0].lower() and 'inspire' in string[0].lower()
    return 'gemet' in name[0].lower() and 'inspire' in name[0].lower()


def is_snimar(context, thesaurus_name):
    """
    Auxiliary method to be used inside schematron

    :param thesaurus_name: string to test
    :type thesaurus_name: `list[str]`
    :return: True if the thesaurus_name is 'SNIMar', False otherwise
    :rtype: bool
    """
    if thesaurus_name is None:
        return False
    snimar_regex = r'^ *thesaurus +snimar +v\.?\d+(\.(x|[0-9]+))? *$'
    return re.match(snimar_regex, thesaurus_name[0], flags=re.IGNORECASE) is not None


def is_serviceclassification(context, string):
    """
    Auxiliary method to be used inside schematron

    :param string: string to test
    :type string: `list[str]`
    :return: True if the string is 'service_classification', False otherwise
    :rtype: bool
    """
    if string is None:
        return False
    return 'iso - 19119 geographic services taxonomy' in string[0].lower()


def is_in_inspirecodelist(context, keyword):
    """
    Auxiliary method to be used inside schematron

    :param keyword: keyword to test
    :type keyword: `list[str]`
    :return: True if belongs to INSPIRE/GEMET list, False otherwise
    :rtype: bool
    """
    if keyword is None:
        return False
    return keyword[0] in inspire


def is_in_serviceclass_codelist(context, keyword):
    """
    Auxiliary method to be used inside schematron

    :param keyword: keyword to test
    :type keyword: `list[str]`
    :return: True if belongs to Service Classification codeList, False otherwise
    :rtype: bool
    """
    if keyword is None:
        return False
    return keyword[0].strip() in serviceclassification


def where_are_codelist_file(context):
    """
    Auxiliary method to be used inside schematron

    :return: the path to gmx codelist
    :rtype: str
    """
    return os.path.join(FILE_PATH, 'SNIMAR_SCHEMATRON/CodeLists/gmxCodelists.xml')
