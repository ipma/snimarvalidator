# coding=utf-8
"""
SNIMar Command Line utility
"""
import argparse
from lxml import etree
from snimarvalidator import __version__ as sn_ver
from snimarvalidator.validator import Validator


def main():
    """This function implements a CLI interface for UNIX environments to use the validator module
    in a terminal

    :return: A string or a boolean depending of the flags that are used
    :rtype: `str or bool`
    """

    header = \
        ur'         ^^       ██╗  ██╗███████╗██╗     ██████╗        ^^          ^     ' + '\n' + \
        ur'                  ██║  ██║██╔════╝██║ ^^  ██╔══██╗                         ' + '\n' + \
        ur'    ~,  ^^   ^   ^███████║█████╗  ██║     ██████╔╝   ~,  ^^          |     ' + '\n' + \
        ur'    /|    ^^      ██╔══██║██╔══╝  ██║ ^   ██╔═══╝    /|    ^^      \ _ /   ' + '\n' + \
        ur'   / |\           ██║  ██║███████╗███████╗██║       / |\        -=  ( )  =-' + '\n' + \
        ur'~ ~=====^~^~-~^~~^╚═╝  ╚═╝╚══════╝╚══════╝╚═╝^~^~  ~=====^~^~-~^~^-=~=~=-~^'
    epilog = u'SNIMar Validator v' + sn_ver + u'\nWithout flags, returns True or False in ' \
             + u'conformance with validity of the Metadata file.'
    parser = argparse.ArgumentParser(add_help=False,
                                     description=header,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=epilog)
    group1 = parser.add_mutually_exclusive_group()
    group1.add_argument("-h", "--help", help="The help for help!", action="help")
    group1.add_argument("-v", "--version", help="Version of this program.", action="version",
                        version='SNIMar Validator v' + sn_ver)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-r", "--report", help="Returns full detailed error repor.t",
                       action="store_true")
    group.add_argument("-e", "--errorcodelist",
                       help="Returns a list of [\"error Code\", \"line\"].", action="store_true")
    parser.add_argument("filepath", help="Path to metadata file to validate.",
                        type=argparse.FileType('r'))
    args = parser.parse_args()
    val = Validator()
    try:
        doc_etree = etree.parse(args.filepath)
    except etree.ParseError as perror:
        print perror.message  # fixme this message  is not fully correct
        return -1
    validation_result = val.validate(doc_etree)
    ret = "Metadata File:" + args.filepath.name + "\n"
    if args.report:  # print report
        ret += val.get_error_report()
        if validation_result:
            ret += "Valid SNIMar Profile Metadata File!"
        else:
            ret += "\nNot Valid SNIMar Profile Metadata File!"
    elif args.errorcodelist:  # print error code list
        buf = val.get_error_codes_list()
        if len(buf) != 0:
            ret += str(buf)
        if validation_result:
            ret += "Valid SNIMar Profile Metadata!"
        else:
            ret += "\nNot Valid SNIMar Profile Metadata File!"
    else:  # True / False (Valid or Not Valid)
        ret = str(validation_result)
    args.filepath.close()
    return ret
