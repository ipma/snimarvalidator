<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <ns prefix="gmd" uri="http://www.isotc211.org/2005/gmd"/>
    <ns prefix="gco" uri="http://www.isotc211.org/2005/gco"/>
    <ns prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
    <ns prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
    <ns prefix="gmx" uri="http://www.isotc211.org/2005/gmx"/>
    <ns prefix="gml" uri="http://www.opengis.net/gml"/>
    <ns prefix="gml32" uri="http://www.opengis.net/gml/3.2"/>
    <ns prefix="snm" uri="http://snimar.pt/customFunctions"/>
    <ns prefix="ns" uri="http://www.w3.org/2001/XMLSchema"/>
    <ns prefix="srv" uri="http://www.w3.org/2001/XMLSchema"/>
    <!--EMPTY_LEAF-->
    <pattern>
        <rule context="//*">
            <let name="cur" value="."/>
            <assert flag="FAIL" id="EMPTY_LEAF" test="count(*[count(child::*) &lt; 1][not(@nilReason)][not(@codeListValue)][not(@xlink:href)][not(normalize-space(text()))]) &lt; 1">It's mandatory to all leaf nodes have a value(except:[@codelistValues,@nilReason,@href)</assert>
        </rule>
    </pattern>
    <!-- -->
    <!--UNKNOWN_CODE_LIST_VALUE-->
    <pattern>
        <rule context="*[@codeList][not(self::gmd:LanguageCode)]">
            <let name="catalog" value="document(snm:where_are_codelist_file(),.)"/>
            <let name="codeList" value="substring-after(@codeList,'#')"/>
            <let name="codeListValue" value="@codeListValue"/>
            <assert flag="FAIL" id="UNKNOWN_CODE_LIST_VALUE" test="$catalog//gmx:codelistItem/*[@gml:id=$codeList or @gml32:id=$codeList]/gmx:codeEntry/*[gml:identifier=$codeListValue or gml32:identifier=$codeListValue]">It's mandatory to use the defined codeLists.This value its not recognized:<value-of select="$codeListValue"/></assert>
        </rule>
    </pattern>
    <!--METADATA_LANGUAGE_NOT_POR-->
    <pattern>
        <rule context="gmd:MD_Metadata/gmd:language">
            <assert flag="FAIL" id="METADATA_LANGUAGE_NOT_POR" test="gmd:LanguageCode/@codeListValue='por'">The language that is used in metadata should be Portuguese('por')[Value used:<value-of select="gmd:LanguageCode/@codeListValue"/>]</assert>
        </rule>
    </pattern>
    <!--NO_METADATA_POINT_OF_CONTACT-->
    <pattern>
        <rule context="gmd:MD_Metadata">
            <assert flag="FAIL" id="NO_METADATA_POINT_OF_CONTACT" test="count(gmd:contact/gmd:CI_ResponsibleParty/gmd:role/gmd:CI_RoleCode[@codeListValue='pointOfContact']) > 0">It's mandatory that the metadata contains a contact with the role 'pointOfContact'</assert>
        </rule>
    </pattern>
    <!--NO_RESOURCE_POINT_OF_CONTACT-->
    <pattern>
        <rule context="gmd:MD_Metadata/gmd:identificationInfo/node()" subject="">
            <assert flag="FAIL" id="NO_RESOURCE_POINT_OF_CONTACT" test="count(gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:role/gmd:CI_RoleCode[@codeListValue='pointOfContact']) > 0">It's mandatory that the resource contains a contact with the role 'pointOfContact'</assert>
        </rule>
    </pattern>
    <!--WRONG_ROLE_FOR_DISTRIBUTOR-->
    <pattern>
        <rule context="gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:distributor/gmd:MD_Distributor/gmd:distributorContact/gmd:CI_ResponsibleParty/gmd:role">
            <assert flag="FAIL" id="WRONG_ROLE_FOR_DISTRIBUTOR" test="gmd:CI_RoleCode/@codeListValue ='distributor'">It's mandatory the use of role 'distributor' in Distributor contact[Incorrect value:<value-of select="gmd:CI_RoleCode/@codeListValue"/>]</assert>
        </rule>
    </pattern>
    <!--SCOPE_AND_IDENTIFICATION_INFO_DISAGREEMENT  -->
    <pattern>
        <let name="scope" value="gmd:MD_Metadata/gmd:hierarchyLevel/gmd:MD_ScopeCode/@codeListValue"/>
        <rule context="gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification">
            <assert flag="FAIL" id="SCOPE_AND_IDENTIFICATION_INFO_DISAGREEMENT" test="$scope='dataset' or $scope='series'">It's mandatory that the scope and gmd:identificationInfo child agree(Current scope:<value-of select="$scope"/>,Current child:<name path="*"/> )</assert>
        </rule>
        <rule context="gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification">
            <assert flag="FAIL" id="SCOPE" test="$scope = 'service'">It's mandatory that the scope and gmd:identificationInfo child agree(Current scope:<value-of select="$scope"/>,Current child:<name path="*"/> )</assert>
        </rule>
    </pattern>
    <!-- NO_PT_FREETEXT_ABSTRACT && MISSING_ENG_PT_FREETEXT_ABSTRACT-->
    <pattern>
        <rule context="/gmd:MD_Metadata/gmd:identificationInfo/node()/gmd:abstract">
            <assert flag="FAIL" id="NO_PT_FREETEXT_ABSTRACT" test="count(gmd:PT_FreeText) &gt; 0">It's mandatory that the node 'gmd:abstract' contains a sub-node 'gmd:PT_Freetext'</assert>
            <assert flag="FAIL" id="MISSING_ENG_PT_FREETEXT_ABSTRACT" test="count(gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString[@locale='locale-en']) &gt; 0">It's mandatory that the node "gmd:abstract" contains sub-node 'gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString' with the value:'locale-en' for the attribute 'locale'.</assert>
        </rule>
    </pattern>
    <!--NO_PT_FREETEXT_RESOURCE_TITLE&&MISSING_ENG_PT_FREETEXT_RESOURCE_TITLE-->
    <pattern>
        <rule context="/gmd:MD_Metadata/gmd:identificationInfo/node()/gmd:citation/gmd:CI_Citation/gmd:title">
            <assert flag="FAIL" id="NO_PT_FREETEXT_RESOURCE_TITLE" test="count(gmd:PT_FreeText) &gt; 0">It's mandatory that the node "gmd:identificationInfo/.../gmd:title" contains a sub-node 'gmd:PT_Freetext'</assert>
            <assert flag="FAIL" id="MISSING_ENG_PT_FREETEXT_RESOURCE_TITLE" test="count(gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString[@locale='locale-en']) &gt; 0">It's mandatory that the node "gmd:identificationInfo/.../gmd:title"contains sub-node 'gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString' with the value:'locale-en' for the attribute 'locale'.</assert>
        </rule>
    </pattern>
    <!--MISSING_OTHER_CONSTR-->
    <pattern>
        <rule context="gmd:MD_LegalConstraints">
            <let name="accessCode" value="gmd:accessConstraints/gmd:MD_RestrictionCode/@codeListValue"/>
            <let name="useCode" value="gmd:useConstraints/gmd:MD_RestrictionCode/@codeListValue"/>
            <assert flag="FAIL" id="MISSING_OTHER_CONSTR" test="gmd:otherConstraints or ($accessCode!='otherRestrictions' and $useCode!='otherRestrictions' )">It's mandatory the use of the element 'gmd:otherConstraints' if the codeValue 'otherRestriction' is used in the siblings nodes.</assert>
        </rule>
    </pattern>
    <!--MISSING_DOMAIN_REPORT-->
    <pattern>
        <rule context="gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:report[1]">
            <assert flag="FAIL" id="MISSING_DOMAIN_REPORT" test="gmd:DQ_DomainConsistency">It's mandatory that the first report contain the node 'gmd:DQ_DomainConsistency'</assert>
        </rule>
    </pattern>
    <!-- SCOPE_DISAGREEMENT -->
    <pattern>
        <!-- FIXME ADD CHECK FOR MD_DATA VS SV_SERVICE VS SCOPES-->
        <let name="metaType" value="gmd:MD_Metadata/gmd:hierarchyLevel/gmd:MD_ScopeCode/@codeListValue"/>
        <rule context="gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:scope/gmd:DQ_Scope">
            <assert flag="FAIL" id="SCOPE_DISAGREEMENT" test="gmd:level/gmd:MD_ScopeCode/@codeListValue = $metaType"/>
        </rule>
    </pattern>
    <!-- MISSING_NODE_LI_SOURCE -->
    <pattern>
        <rule context="//gmd:LI_Source">
            <assert flag="FAIL" id="MISSING_NODE_LI_SOURCE" test="gmd:description or gmd:sourceExtent"/>
        </rule>
    </pattern>
    <!-- OUT_OF_BOUNDS_(WEST|EAST|NORTH|SOUTH) -->
    <pattern>
        <rule context="gmd:EX_GeographicBoundingBox">
            <let name="west" value="number(gmd:westBoundLongitude/gco:Decimal)"/>
            <let name="east" value="number(gmd:eastBoundLongitude/gco:Decimal)"/>
            <let name="south" value="number(gmd:southBoundLatitude/gco:Decimal)"/>
            <let name="north" value="number(gmd:northBoundLatitude/gco:Decimal)"/>
            <assert flag="FAIL" id="OUT_OF_BOUNDS_WEST" test="-180.0 &lt;= $west and $west &lt;= 180.0"/>
            <assert flag="FAIL" id="OUT_OF_BOUNDS_EAST" test="-180.0 &lt;= $east and $east &lt;= 180.0 and $west &lt;= $east "/>
            <assert flag="FAIL" id="OUT_OF_BOUNDS_SOUTH" test="-90.0 &lt;= $south and $south &lt;=90.0"/>
            <assert flag="FAIL" id="OUT_OF_BOUNDS_NORTH" test="-90.0 &lt;= $north and $north &lt;= 90.0 and $south &lt; $north "/>
        </rule>
    </pattern>
    <!--OUT_OF_BOUNDS_VERTICAL_EXT-->
    <pattern>
        <rule context="gmd:Ex_VerticalExtent">
            <let name="min" value="number(gmd:minimumValue/gco:Real)"/>
            <let name="max" value="number(gmd:maximumValue/gco:Real)"/>
            <assert flag="OUT_OF_BOUNDS_VERTICAL_EXT" id="snv-25" test="$min &lt; $max"/>
        </rule>
    </pattern>
    <!--DATE_IN_FUTURE-->
    <pattern>
        <rule context="//*/gco:Date">
            <assert flag="FAIL" id="DATE_IN_FUTURE" test="snm:date_in_future(./text())">Incorrect Value:<value-of select="./text()"/></assert>
        </rule>
    </pattern>
    <!--DATE_TIME_IN_FUTURE-->
    <pattern>
        <rule context="//*/gco:DateTime | //*/gml:TimePeriod/* ">
            <assert flag="FAIL" id="DATE_TIME_IN_FUTURE" test="snm:datetime_in_future(./text())">Incorrect Value:<value-of select="./text()"/></assert>

        </rule>
    </pattern>
    <!--WRONG_TIME_PERIOD-->
    <pattern>
        <rule context="//*/gml:TimePeriod ">
            <assert flag="FAIL" id="WRONG_TIME_PERIOD" test="snm:check_time_extension(gml:beginPosition/text(),gml:endPosition/text())"/>
        </rule>
    </pattern>
    <!--NO_HAVE_REQUIRED_INSPIRE_KEYWORD-->
    <pattern>
        <rule context="gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification">
            <assert flag="FAIL" id="NO_HAVE_REQUIRED_INSPIRE_KEYWORD" test="count(gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString[snm:is_inspire(./text())]) &gt; 0">NO INSPIRE </assert>
        </rule>
    </pattern>
    <!--KEYWORD_DO_NOT_BELONG_TO_INSPIRE_PT-->
    <pattern>
        <rule context="//*/gmd:MD_Keywords[gmd:thesaurusName][snm:is_inspire(gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString/./text())]">
            <assert flag="FAIL" id="KEYWORD_DO_NOT_BELONG_TO_INSPIRE_PT" test="snm:is_in_inspirecodeList(gmd:keyword/gco:CharacterString/text())">One or more words are marked as INSPIRE but don't belong INSPIRE(PT) codelists</assert>
        </rule>
    </pattern>
    <!--KEYWORD_DO_NOT_BELONG_TO_SERVICE_CLASSIFICATION-->
    <pattern>
        <rule context="gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification">
            <assert flag="FAIL" id="KEYWORD_DO_NOT_BELONG_TO_SERVICE_CLASSIFICATION" test="count(gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString[snm:is_serviceclassification(./text())]) &gt; 0">cenas</assert>
        </rule>
    </pattern>
    <!--WRONG_NR_SN_KWORDS|MISSING_SN_DISCP|MISSING_SN_PARAM-->
    <pattern>
        <rule context="gmd:MD_Metadata/gmd:identificationInfo/node()">
            <assert flag="FAIL" id="WRONG_NR_SN_KWORDS" test="count(gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString[snm:is_snimar(./text())]) &gt; 1">It's mandatory the use at least 2 snimar keywords</assert>
            <assert flag="FAIL" id="MISSING_SN_DISCP" test="count(gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:type/gmd:MD_KeywordTypeCode[@codeListValue='discipline' and  snm:is_snimar(../../gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString/text())]) &gt; 0">Its mandatory to use one SNIMar keyword of type 'discipline'</assert>
            <assert flag="FAIL" id="MISSING_SN_PARAM" test="count(gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:type/gmd:MD_KeywordTypeCode[@codeListValue='parameter' and  snm:is_snimar(../../gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString/text())]) &gt; 0">Its mandatory to use one SNIMar keyword of type 'parameter'</assert>
        </rule>
    </pattern>
    <!-- ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::. -->
</schema>