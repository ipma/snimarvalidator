"""
TODO
"""
from setuptools import setup, find_packages
from snimarvalidator import __version__ as sn_version

reqs = [line.strip() for line in open('requirements.txt')]

setup(
    name="snimarvalidator",
    version=sn_version,
    description="Validator for XML metadata SNIMar profile",
    long_description=open('README.md').read(),
    license="Apache License 2.0",
    keywords="snimar xml validation metadata",
    author="Eduardo Castanho",
    author_email="eduardo.castanho@ipma.pt",
    maintainer="Eduardo Castanho",
    maintainer_email="eduardo.castanho@ipma.pt",
    url="https://bitbucket.org/ipma/snimarvalidator",
    install_requires=reqs,
    entry_points={
        'console_scripts': ['snimarvalidator-cli=snimarvalidator.command_line:main']
    },
    packages=find_packages(exclude=['tests']) + ['snimarvalidator/SNIMAR_PROFILE_XSD',
                                                 'snimarvalidator/SNIMAR_SCHEMATRON'],
    include_package_data=True,
    package_data={
        '': ['*.*']
    },
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Intended Audience :: End Users/Desktop',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Natural Language :: English',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 2.7',
    ],
    zip_safe=False,
    requires=['lxml'],
)
